﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HWSRS.Core.Migrations
{
    public partial class Now : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TextBooks",
                columns: table => new
                {
                    TextbookId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Author = table.Column<string>(nullable: true),
                    Edition = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TextBooks", x => x.TextbookId);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    ReviewId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ReviewDate = table.Column<DateTime>(nullable: false),
                    Correct = table.Column<bool>(nullable: false),
                    QuestionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => x.ReviewId);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TextbookId = table.Column<int>(nullable: false),
                    Chapter = table.Column<int>(nullable: true),
                    SubChapter = table.Column<int>(nullable: true),
                    Number = table.Column<int>(nullable: true),
                    Correct = table.Column<bool>(nullable: false),
                    LastReviewId = table.Column<int>(nullable: true),
                    QuestionString = table.Column<string>(nullable: true),
                    AnswerString = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.QuestionId);
                    table.ForeignKey(
                        name: "FK_Questions_Reviews_LastReviewId",
                        column: x => x.LastReviewId,
                        principalTable: "Reviews",
                        principalColumn: "ReviewId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Questions_TextBooks_TextbookId",
                        column: x => x.TextbookId,
                        principalTable: "TextBooks",
                        principalColumn: "TextbookId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Questions_LastReviewId",
                table: "Questions",
                column: "LastReviewId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_TextbookId",
                table: "Questions",
                column: "TextbookId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_QuestionId",
                table: "Reviews",
                column: "QuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reviews_Questions_QuestionId",
                table: "Reviews",
                column: "QuestionId",
                principalTable: "Questions",
                principalColumn: "QuestionId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Questions_Reviews_LastReviewId",
                table: "Questions");

            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "TextBooks");
        }
    }
}
