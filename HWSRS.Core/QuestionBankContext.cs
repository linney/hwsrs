using Microsoft.EntityFrameworkCore;

namespace HWSRS.Core
{
    public class QuestionBankContext : DbContext
    {
        public DbSet<Textbook> TextBooks { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Review> Reviews { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Question>()
                .HasOne(q => q.Textbook)
                .WithMany(tb => tb.Questions);

            modelBuilder.Entity<Review>()
                .HasOne(r => r.Question)
                .WithMany(q => q.Reviews);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=/home/jacob/Questions.db");
        }
    }
}