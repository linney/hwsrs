using System;
using System.Collections.Generic;

namespace HWSRS.Core
{
    public class Question
    {
        public int QuestionId { get; set; }

        public int TextbookId { get; set; }
        public Textbook Textbook { get; set; }
        
        public int? Chapter { get; set; }
        public int? SubChapter { get; set; }
        public int? Number { get; set; }
        
        public bool Correct { get; set; }
        
        public int? LastReviewId { get; set; }
        public Review LastReview { get; set; }
        
        public string QuestionString { get; set; }
        public string AnswerString { get; set; }
        
        public List<Review> Reviews { get; set; }

        public override string ToString()
        {
            return $"{Textbook}: Q. {Number}, Ch. {Chapter}.{SubChapter}";
        }
    }
}