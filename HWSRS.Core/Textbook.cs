using System.Collections.Generic;

namespace HWSRS.Core
{
    public class Textbook
    {
        public int TextbookId { get; set; }
        
        public string Name { get; set; }
        public string Author { get; set; }
        public int Edition { get; set; }
        
        public List<Question> Questions { get; set; }

        public override string ToString()
        {
            return $"{Name} ({Edition} Ed.) by {Author}";
        }
    }
}