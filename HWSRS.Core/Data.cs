using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Serilog.Core;

namespace HWSRS.Core
{
    public static class Data
    {
        private static bool _initialised;
        private static Logger _log;
        private static QuestionBankContext _db;
        
        private static void Initialise()
        {
            _db = new QuestionBankContext();
            _log = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.File("logs/log.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            _log.Information("HWSRS.Core.Data was initialised successfully.");
            _initialised = true;
        }

        // This function should be called on application shutdown.
        public static void Exit(object sender = null, EventArgs e =null)
        {
            _log.Information("Shutting down.");
            _db.Dispose();
            _initialised = false;
        }

        public static void AddTextbook(Textbook textbook)
        {
            if (!_initialised)
                Initialise();
            
            _log.Verbose("Attempting to add new textbook to the database.");

            if (CheckForTextbook(textbook))
            {
                _log.Warning($"Textbook: ({textbook}) was already found, not adding to database again.");
                return;
            }

            try
            {
                _db.TextBooks.Add(textbook);
                _db.SaveChanges();
                _log.Information($"Textbook: ({textbook}) successfully added to the database.");
            }
            catch (Exception e)
            {
                _log.Error($"Textbook ({textbook}) could not be added to our database! {e.Message}");
                throw;
            }
        }
       
        public static void CreateQuestion(Textbook textbook, Question question, bool saveChanges = true)
        {
            if (!_initialised)
                Initialise();
            
            // TODO: This is just stupid, but does get rid of unnecessary logging in my use case.
            //     Batch question creation does not need to repeatedly log this.
            //     Maybe calling 'saveChanges' something like 'commit transaction' would be better.
            if (saveChanges)
                _log.Verbose("Preparing to add new question to the database.");

            // Checking that the related textbook is in our database.
            // TODO: We have a helper method for getting textbooks with questions, but I don't think we want the memory
            //       overhead of that here.
            var foundTextbook = _db.TextBooks.FirstOrDefault(t => t.Name == textbook.Name
                                                                  && t.Author == textbook.Author
                                                                  && t.Edition == textbook.Edition);
            if (foundTextbook == null)
            {
                _log.Error($"Textbook ({textbook}) was not found in the database. The question ({question}) could not be added.");
                
                // TODO: Better Exceptions
                throw new Exception($"Textbook not found: {textbook}");
            }

            question.TextbookId = foundTextbook.TextbookId;
            question.Textbook = null;

            // Checking all of the necessary information for question is there.
            if (question.Chapter == null
                || question.SubChapter == null
                || question.Number == null)
            {
                _log.Error($"Question ({question}) is missing required info and could not be added to the database.");
                
                // TODO: Better Exceptions
                throw new ArgumentNullException($"Question: {question}");
            }
            
            // Checking if the question already exists.
            if (_db.Questions.Any(q => q.Chapter == question.Chapter
                                       && q.SubChapter == question.SubChapter
                                       && q.Number == question.Number
                                       && q.TextbookId == question.TextbookId))
            {
                _log.Warning($"Question ({question}) is already in our database.");
                return;
            }

            
            try
            {
                _db.Add(question);
                if (saveChanges)
                {
                    _db.SaveChanges();
                    _log.Information($"Question ({question}) was successfully added to the database.");
                }
            }
            catch (Exception e)
            {
                _log.Error($"Question ({question}) could not be added to our database! {e.Message}");
                throw;
            }
        }

        public static void CreateQuestionsBatch(Textbook textbook, int chapter, int subChapter, int minQuestion,
            int maxQuestion)
        {
            if (!_initialised)
                Initialise();

            _log.Information($"Attempting to add questions {minQuestion}-{maxQuestion} to chapter {chapter}.{subChapter} of {textbook}");
            
            if (minQuestion > maxQuestion)
            {
                _log.Error("Minimum question number was less than the maximum!");
                
                throw new ArgumentException("Minimum question number was less than the maximum.");
            }

            for (var i = minQuestion; i < maxQuestion; i++)
            {
                CreateQuestion(textbook, new Question{ Number = i, Chapter = chapter, SubChapter = subChapter}, false);
            }

            try
            {
                _db.SaveChanges();
                _log.Information("Questions saved to database successfully.");
            }
            catch (Exception e)
            {
                _log.Error($"Questions failed to be added to the database! {e.Message}");
                throw;
            }
        }


        public static List<Question> GetQuestions(Textbook textbook, int chapter = -1, int subChapter = -1)
        {
            if (!_initialised)
                Initialise();

            IEnumerable<Question> questions = GetTextbookWithQuestions(textbook).Questions;
            
            
            if (chapter >= 0)
            {
                questions = questions.Where(q => q.Chapter == chapter);
            }

            if (subChapter >= 0)
            {
                questions = questions.Where(q => q.SubChapter == subChapter);
            }

            return questions.ToList();
        }

        private static bool CheckForTextbook(Textbook textbook)
        {
            if (!_initialised)
                Initialise();
            
            if (textbook.Author == null
                || textbook.Name == null)
            {
                _log.Error($"Textbook ({textbook}) is missing required info and could not be found.");
                
                // TODO: Better Exceptions
                throw new ArgumentNullException($"Textbook: {textbook}");
            }
            
            if (_db.TextBooks.Any(t => t.Name == textbook.Name
                                       && t.Author == textbook.Author
                                       && t.Edition == textbook.Edition))
            {
                _log.Verbose($"Textbook ({textbook}) was found in the database.");
                return true;
            }
            _log.Verbose($"Textbook ({textbook}) was not found in the database.");
            return false;
        }

        private static Textbook GetTextbookWithQuestions(Textbook textbook)
        {
            if (!_initialised)
                Initialise();
            
            if (!CheckForTextbook(textbook))
            {
                _log.Error($"Textbook: ({textbook}) was not found in the database!");
                return null;
            }

            textbook = _db.TextBooks.Include(t => t.Questions)
                .FirstOrDefault(t => t.Name == textbook.Name
                                     && t.Author == textbook.Author
                                     && t.Edition == textbook.Edition);

            return textbook;
        }

        public static void ReviewQuestion(Question question, bool correct)
        {
            if (!_initialised)
                Initialise();

            if (question.QuestionId != 0)
                question = _db.Questions
                    .Include(q => q.Reviews)
                    .SingleOrDefault(q=> q.QuestionId == question.QuestionId);
            else
                question =_db.Questions
                    .Include(q => q.Reviews)
                    .FirstOrDefault(q => q.Chapter == question.Chapter
                                         && q.SubChapter == question.SubChapter
                                         && q.Number == question.Number
                                         && q.TextbookId == question.TextbookId);

            if (question == null)
            {
                _log.Error($"Cannot review question: Question was not found in the database!");
                
                // TODO: Error Cleanup
                throw new NullReferenceException($"Question does not exist!");
            }
            
            var review = new Review
            {
                QuestionId = question.QuestionId,
                Correct = correct,
                ReviewDate = DateTime.UtcNow,
            };

            question.Reviews.Append(review);
            question.LastReview = review;
            
            try
            {
                _db.Reviews.Add(review);
                _db.Questions.Update(question);
                _db.SaveChanges();
                _log.Information($"Question ({question}): was successfully reviewed.");
            }
            catch (Exception e)
            {
                _log.Information($"Question ({question}): was not successfully reviewed! {e.Message}");
            }
        }

        public static void ReviewQuestion(int questionId, bool correct)
        {
            ReviewQuestion(new Question{ QuestionId = questionId}, correct);
        }
    }
}