using System;

namespace HWSRS.Core
{
    public class Review
    {
        public int ReviewId { get; set; }
        
        public DateTime ReviewDate { get; set; }
        public bool Correct { get; set; }
        
        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}